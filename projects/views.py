from django.shortcuts import render
from .models import Project
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def list_projects(request):
    list_of_projects = Project.objects.filter(owner=request.user)  # returns all the projects for us
    context = {
        "projects": list_of_projects,
    }
    return render(request, "projects/list.html", context)